# reveal.js <br> An Introduction



## Introduction: reveal.js

- uses only ascii but renders pretty presentations, since it can contain
  - equations
  - graphics
  - tables
- can be hosted on GitLab and therefore:
  - used to collaboratively improve slides
  - always have a backup
- easy to maintain thanks to Markdown

Note: Briefly introduce main features of reveal.js


## Main features: Equation

The following $\LaTeX$ code  
```LaTeX
\[ J(\theta_0,\theta_1) = \sum_{i=0} \]
```

will give
`\[ J(\theta_0,\theta_1) = \sum_{i=0} \]`


## Main features: Graphics

The following `Markdown` code  
```Markdown
![External Image](educarte.png)
```
will give

![External Image](educarte.png)

Note: This slide does not yield the correct output


## Main features: Tables

Tables are generated in standard `Markdown` language:
|Country |Year  |GDP growth <br> (in %) | 
| :----: | ---- | :------:   |   
| Luxembourg | 2022      | 2.0 |  
| Germany    | 2021      | 2.9 |




## Installing and using reveal.js

- Current presentation: local installation of reveal.js
- Other possibilities: host reveal.js slides on GitLab pages




## External 3.1

Content 3.1


## External 3.2

Content 3.2


## External 3.3 (Image)

![External Image](https://s3.amazonaws.com/static.slid.es/logo/v2/slides-symbol-512x512.png)


## External 3.4 (Math)

`\[ J(\theta_0,\theta_1) = \sum_{i=0} \]`
