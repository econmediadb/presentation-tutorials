# Presentations

## 1. Templates

[Template](https://econmediadb.gitlab.io/presentation-tutorials/revealjs/myslides/00-markdown-original.html)

## 2. Sciences économiques

[Comptabilité générale (3DG)](https://econmediadb.gitlab.io/presentation-tutorials/revealjs/myslides/01-3DG-comptabilite.html)

[Economie politique (3DG)](https://econmediadb.gitlab.io/presentation-tutorials/revealjs/myslides/02-3DG-economie-politique.html)

### 2.1. Economie politique 

[Vidéos pédagogiques](https://gitlab.com/econmediadb/economics-course/-/blob/main/markdown/3e-videos.md#chapitre-1-la-science-économique-une-science-de-choix)

[Chapitre 1 : La science économique – une science de choix](https://gitlab.com/econmediadb/economics-course/-/blob/main/markdown/3e-concept-chapitre-1.md#chapitre-1-la-science-économique-une-science-de-choix)

[Chapitre 2 : Comment crée-t-on des richesses et comment les mesure-t-on ?](https://gitlab.com/econmediadb/economics-course/-/blob/main/markdown/3e-concept-chapitre-2.md#chapitre-2-comment-crée-t-on-des-richesses-et-comment-les-mesure-t-on-)

[Chapitre 3 : Comment se forment les prix sur un marché ?](https://gitlab.com/econmediadb/economics-course/-/blob/main/markdown/3e-concept-chapitre-3.md#chapitre-3-comment-se-forment-les-prix-sur-un-marché-)

## 3. Soziologie 

### 2e

[Die soziale Gruppe](https://gitlab.com/econmediadb/sociology/-/blob/main/markdown/2e_8_soziale_gruppe.md#die-soziale-gruppe)
